<?php

class DashboardController {
    
    public $produtos;
    
    public function __construct() {
        $this->carregarLista();
    }
    
    function carregarLista(){
        try {
        $con = new dbController();
        $conexao = $con->conexao();
        $produtos = $conexao->query("SELECT * FROM produtos");
        $lista = array();
        $this->produtos = $produtos->fetchAll();
        } catch (Exception $ex) {
            echo $ex->getMessage();   
        }
    }

    function listar(){
         $listarProdutos = $this->produtos;
        require __DIR__."/../../view/dashboard.php";
        
    }

}
