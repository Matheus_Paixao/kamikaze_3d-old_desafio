<?php

require __DIR__ . '/../autoloader.php';

class ProdutoController {

    public $produtos;

    public function __construct() {
        $this->carregarLista();
    }

    function carregarLista() {
        try {
            $conexao = $this->conexao();
            $produtos = $conexao->query("SELECT * FROM produtos");
            $lista = array();
            $this->produtos = $produtos->fetchAll();
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    function conexao() {
        $con = new dbController();
        $conexao = $con->conexao();
        return $conexao;
    }
    
    function editaProduto($sku,$dados){

        try {
            $db = $this->conexao();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

            $query = $db->prepare("UPDATE produtos SET nome = :nome WHERE sku = {$sku}");
            $query->execute();

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    function deletaProduto($sku){
        try {
            $db = $this->conexao();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

            $query = $db->prepare("DELETE FROM produtos WHERE sku = :sku");
            $query->bindParam(':sku', $sku);
            $query->execute();
            header("Location: /produtos");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    function criaProduto($nome, $img, $sku, $descricao, $quantidade, $preco, $categorias) {
        try {
            $db = $this->conexao();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

            $query = $db->prepare('INSERT INTO produtos( nome,img,sku,descricao,quantidade,preco,categorias) VALUES(:nome,:img,:sku,:descricao,:quantidade,:preco,:categorias)');
            $query->execute(array(
                ':nome' => $nome,
                ':img' => $img,
                ':sku' => $sku,
                ':descricao' => $descricao,
                ':quantidade' => $quantidade,
                ':preco' => $preco,
                ':categorias' => $categorias,
            ));
            header("Location: /");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    function listar() {
        $listarProdutos = $this->produtos;
        require __DIR__ . "/../../view/produto/produtos.php";
    }

}
