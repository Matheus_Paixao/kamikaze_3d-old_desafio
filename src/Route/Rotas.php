<?php

$path = $_SERVER['REQUEST_URI'];
require __DIR__ . '/../autoloader.php';

$banco = __DIR__ . '/../../db/' . 'gojump.sqlite';

if (is_file($banco)) {
    switch ($path) {
        case "/":
            $index = new dbController();
            $index->verificaInstalacao();
            break;
        case "/produtos":
            $produtos = new ProdutoController();
            $produtos->listar();
            break;
        case "/carregaImagem":
            $diretorio = "assets/images/product/";
            chmod("assets/images/product/", 0755);
            move_uploaded_file($_FILES['imagem']["tmp_name"], $diretorio . $_FILES['imagem']["name"]);
            var_dump($_FILES['imagem']["tmp_name"]);
            $_POST['imagem'] = $_FILES['imagem']["name"];

            $salvaProduto = new ProdutoController();

            $nome = $_POST['nome'];
            $img = $_POST['imagem'];
            $sku = $_POST['sku'];
            $descricao = $_POST['descricao'];
            $quantidade = $_POST['quantidade'];
            $preco = $_POST['preco'];
            $categorias = $_POST['categorias'];
            $salvaProduto->criaProduto($nome, $img, $sku, $descricao, $quantidade, $preco, $categorias);

            header("Location: /");
            break;
        case "/install":
            require __DIR__ . '/../../view/install/database.php';
            break;
        case "/importar":
            require __DIR__ . '/../../view/install/importaProdutos.php';
            break;
        case "/criartabela":
            $tabela = new dbController();
            $tabela->verificaInstalacao();
            break;
        case "/novo-produto":
            $novoProduto = new NovoProdutoController();
            $novoProduto->listar();
            break;
        case "/salvaproduto":
            $salvaProduto = new ProdutoController();
            $diretorio = "assets/images/product/";
            chmod("assets/images/product/", 0755);
            move_uploaded_file($_FILES['imagem']["tmp_name"], $diretorio . $_FILES['imagem']["name"]);
            $_POST['imagem'] = $_FILES['imagem']["name"];
            $nome = $_POST['nome'];
            $img = $_POST['imagem'];
            $sku = $_POST['sku'];
            $descricao = $_POST['descricao'];
            $quantidade = $_POST['quantidade'];
            $preco = $_POST['preco'];
            $categorias = $_POST['categorias'];
            $salvaProduto->criaProduto($nome, $img, $sku, $descricao, $quantidade, $preco, $categorias);
            break;
        case "/deletaproduto":
            $sku = $_POST['sku'];
            $produto = new ProdutoController();
            print_r($sku);
            echo '<br/>';
            $produto->deletaProduto($sku);
            break;
        case "/categorias":
            $categorias = new CategoriaController();
            $categorias->listar();
            break;
        case "/nova-categoria":
            $novaCategoria = new NovaCategoriaController();
            $novaCategoria->listar();
            break;
        case "/dashboard":
            $dashboard = new DashboardController();
            $dashboard->listar();
            break;
        default:
            echo '<p>404</p>';
            break;
    }
} else {
    switch ($path) {
        case "/install":
            require __DIR__ . '/../../view/install/database.php';
            break;
        case "/importar":
            require __DIR__ . '/../../view/install/importaProdutos.php';
        case "/padrao":
            require __DIR__ . '/../../view/install/padrao.php';
            break;
        default:
            header("Location: /install");
            break;
    }
}