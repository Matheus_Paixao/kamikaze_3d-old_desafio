<?php include __DIR__."/../components/header.php"; ?>
 <!-- Main Content -->
 
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Produtos</h1>
      <a href="/novo-produto" class="btn-action">Adicionar produto</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Nome</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Descrição</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quant.</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Preço</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categorias</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Acões</span>
        </th>
      </tr>
      <?php 
      
        foreach ($listarProdutos as $item){ ?>
      
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $item['nome'] ;?></span>
        </td>
      
        <td class="data-grid-td" style="width: 110px;">
           <span class="data-grid-cell-content"><?= $item['sku'] ;?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $item['descricao'] ;?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $item['quantidade'] ;?></span>
        </td>
        
        <td class="data-grid-td" style="width: 120px;">
           <span class="data-grid-cell-content" >R$ <?= $item['preco'] ;?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $item['categoria'] ;?></span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit"><span>Edit</span></div>
            <div class="action delete">
                <form action="/deletaproduto" method="POST" >
                    <input  name="sku" value="<?= $item['sku'];?>" type="text">
                    <input type="submit" value="Delete" name="skus" />
                </form>
            </div>
          </div>
        </td>
      </tr>
       <?php }  ?>
      
    </table>
  </main>
  <!-- Main Content -->
<?php include __DIR__."/../components/footer.php"; ?>