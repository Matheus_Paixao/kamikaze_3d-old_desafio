<?php include __DIR__."/components/header.php"; ?>
<!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
        
        
        You have <?= count($listarProdutos);?> products added on this store: <a href="/novo-produto" class="btn-action">Add new Product</a>
     
    </div>
    <ul class="product-list">
        <?php 
        foreach ($listarProdutos as $item){ ?>
      <li>
        <div class="product-image">
          <img src="assets/images/product/<?= $item['img'];?>" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
        </div>
        <div class="product-info">
          <div class="product-name"><span><?= $item['nome'];?></span></div>
          <div class="product-price"><span class="special-price"><?= $item['quantidade'];?> available</span> <span>R$<?= $item['preco'];?></span></div>
        </div>
      </li>
         <?php } ?>
    </ul>
  </main>
  <!-- Main Content -->
  
<?php include __DIR__."/../view/components/footer.php"; ?>