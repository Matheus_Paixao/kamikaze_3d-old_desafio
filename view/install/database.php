<?php include __DIR__."/../components/head.php"; ?>
<header>
  <a href="index.php" class="link-logo"><img src="assets/images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
</header>  
<main class="content">
    <div class="header-list-page">
      <h1 class="title">Bem vindo</h1>
    </div>
    <div class="infor">
<p>Esta é a área de configuração de uma nova instalação da loja de produtos.</p>
<p>Você pode importar uma base de dados ou fazer a instalação padrão.</p>
<h3>Importar lista</h3>
<form action="/importar" method="POST" enctype="multipart/form-data">
      <div class="input-field">
        <label for="importar" class="label">Importe arquivos do tipo 'CSV' seguindo a estrutura abaixo.</label>
        <input type="file" name="importar" id="importar"  class="input-text" /> 
      </div>
    <small>nome;img;sku;descricao;quantidade;preco;categoria</small>
      <div class="actions-form">
        <span></span>
        <input class="btn-submit btn-action" type="submit" value="Importar Produtos" />
      </div>
    </form>
<form action="/padrao" method="POST" enctype="multpart/form-data" >
      <div class="input-field">
       Ou clique no botão abaixo para fazer a instalação usando o arquivo de exemplo.
      </div>
    <div class="actions-form">
        <span></span>
        <input class="btn-submit btn-action" type="submit" value="Instalar exemplo" />
      </div>

</form>
</div>
</main>
<?php include __DIR__."/../components/footer.php"; ?>