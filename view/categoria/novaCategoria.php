<?php include __DIR__."/../components/header.php"; ?>
<!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Nova Categoria</h1>
    
    <form>
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="category-name" class="input-text" />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" id="category-code" class="input-text" />
        
      </div>
      <div class="actions-form">
        <a href="/categorias" class="action back">Voltar</a>
        <input class="btn-submit btn-action"  type="submit" value="Salvar" />
      </div>
    </form>
  </main>
  <!-- Main Content -->
<?php include __DIR__."/../components/footer.php"; ?>